#ifndef SYSTEM_H
#define SYSTEM_H

#include <xc.h>
#include <stdint.h>

#define _XTAL_FREQ  500000

/**
 * void init_Oscillator(void);
 *  - Initializes the internal oscillator based on the _XTAL_FREQ provided
 */
void init_Oscillator(void);

#endif