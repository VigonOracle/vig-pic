#include "user.h"

void init_App(void){

/*****************
 * U S E R   C O N F I G   for  I/O   P I N S
 * [except CPS that will be set/overwritten on initCPS()]
 * write your code here
 *****************/

    /* Setup user I/O */
    
    TRISA = 0;
    ANSELA = 0; //Put analog pins to digital by default... (to prevent omission)

#if defined(_16F1823)
    TRISC = 0;
    ANSELC = 0; //Put analog pins to digital by default... (to prevent omission)
#endif

    // Timer 2 is PWM
    T2CON   = 0b00000100;  // prescaler 1,timer 2 on
        /*
         * bit  7           : unimplemented
         * bits 6-3 T2OUTP  : postscaler (direct binary value)
         * bit  2           : TMR2 is on
         * bit  1-0 T2CKPS  : prescaler (00:1, 01:4, 10:16, 11:64)
         */
    PR2     = 0b01111100;  // ~1 kHz freq @ 500kHz
    CCP1CON = 0b00001100;  // set for zero initially
    CCPR1L  = 0b00000000;
    
    init_CPS();
    
    GIE     = 1; // enable global interrupts
}