/**
 * C P S   T o u c h
 *  
 * A quite adaptative code to handle CPS detection based on MICROCHIP processors
 *  - Will adapt the code based on a list of used CPS channels (set in CPS.h)
 *  - Up to 8 channels may be handled
 *  - Will adapt the sensitivity for CPS detection (set in CPS.h)
 *  - Will reduce the CPS power or time base to avoid overflows (set in CPS.h)
 *  - Will pseudo-average the CPS value to prevent small/slow changes to trigger
 *  - Will detect sudden change and set the corresponding CPS status
 *      (UP/PRESSED/DOWN/RELEASED) 
 * 
 * @author : Nicolas H.-P. De Coster (Vigon)
 * @date   : 2019-05
 * @version: v0
 * @processor : this code will/should work on:
 *      - PIC12F1822 [tested]
 *      - PIC12F1840 [tested]
 *      - PIC16F1823 [not tested yet] but very similar to 12F1822
 *      - PIC16F1824 [not tested]
 *      - PIC16F1825 [not tested]
 *      - probably more equipped with microchip CPS
 * @todo :
 *  - Try to work with MF oscillator instead of HF (doesn't work so far)
 *  - At startup, try to "calibrate" TMR1 CPS to maximize the counts
 *      (already calibrates TMR1 but only by decreasing the counts)
 *  - 
 */

#include <xc.h>
#include <stdint.h>

#include "system.h" 
#include "user.h"

void main(void)
{
    // Declare variables
    uint8_t  i;
    uint8_t  level;
    
    // Configure oscillator
    init_Oscillator();

    // Initialize I/O and Peripherals for application
    init_App();
    
    
    level   = 0;
    
    //Main loop
    while(1)
    {
        for(i=0;i<CPS_NUM;i++){
            if(CPS[i].status == PRESSED){
                if((level>0 && countDown==0) || level++>=4){
                    level=0;
                }
                switch(level){
                    case 0:// 0%
                        CCP1CON = 0b00001100;
                        CCPR1L  = 0b00000000;
                        break;
                    case 1://100%
                        CCP1CON = 0b00001100;
                        CCPR1L  = 0b01111101;
                        break;
                    case 2://66%
                        CCP1CON = 0b00101100;
                        CCPR1L  = 0b01010010;
                        break;
                    case 3://33%
                        CCP1CON = 0b00011100;
                        CCPR1L  = 0b00101001;
                        break;
                    case 4://10%
                        CCP1CON = 0b00101100;
                        CCPR1L  = 0b00001100;
                        break;
                }
                // Launch a countDown for corresponding TMR0 overflow intervals
                if(level){countDown=35;}
                // Say that detection was handled
                CPS[i].status &= ~(1<<1); //force do DOWN or UP by clearing bit1
            }
        }
    }
}