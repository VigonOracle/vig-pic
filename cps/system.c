#include "system.h"

void init_Oscillator(void){
        /* IRCF :
        * 1111 = 16MHz	HF
        * 1110 = 8 or 32 MHz HF (depending on PLL)
        * 1101 = 4 MHz	HF
        * 1100 = 2MHz	HF
        * 1011 = 1MHz	HF
        * 1010 = 500kHz	HF
        * 1001 = 250kHz HF
        * 1000 = 125kHz HF
        * 0111 = 500kHz MF
        * 0110 = 250kHz MF
        * 0101 = 125kHz MF
        * 0100 = 62.5kHz MF
        * 0011 = 31.25kHz HF
        * 0010 = 31.25kHz MF
        * 000x = 31 kHz LF
        */
#if _XTAL_FREQ==32000000
    OSCCONbits.SPLLEN   = 1;
    OSCCONbits.IRCF     = 0b1110;

#elif _XTAL_FREQ==16000000
    OSCCONbits.SPLLEN   = 0;
    OSCCONbits.IRCF     = 0b1111;

#elif _XTAL_FREQ==8000000
    OSCCONbits.SPLLEN   = 0;
    OSCCONbits.IRCF     = 0b1110;

#elif _XTAL_FREQ==4000000
    OSCCONbits.SPLLEN   = 0;
    OSCCONbits.IRCF     = 0b1101;

#elif _XTAL_FREQ==2000000
    OSCCONbits.SPLLEN   = 0;
    OSCCONbits.IRCF     = 0b1100;

#elif _XTAL_FREQ==1000000
    OSCCONbits.SPLLEN   = 0;
    OSCCONbits.IRCF     = 0b1011;

#elif _XTAL_FREQ==500000
    OSCCONbits.SPLLEN   = 0;
    OSCCONbits.IRCF     = 0b1010;

#elif _XTAL_FREQ==250000
    OSCCONbits.SPLLEN   = 0;
    OSCCONbits.IRCF     = 0b1001;

#elif _XTAL_FREQ==125000
    OSCCONbits.SPLLEN   = 0;
    OSCCONbits.IRCF     = 0b1000;

#elif _XTAL_FREQ==62500
    OSCCONbits.SPLLEN   = 0;
    OSCCONbits.IRCF     = 0b0100;

#elif _XTAL_FREQ==31000
    OSCCONbits.SPLLEN   = 0;
    OSCCONbits.IRCF     = 0b0000;
    
#endif
    while (!HFIOFS) continue; // wait for stable osc
}
