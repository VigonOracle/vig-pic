#include <xc.h>         /* XC8 General Include File */
#include "user.h"
#include "CPS.h"

/**
* void interrupt isr(void)
* Interrupt routine 
* Determines which flag was set and takes corresponding action
*/
void __interrupt() isr(void){
    uint16_t val;
    GIE = 0;  // disable interrupts
    
    /* Did TMR 0 triggered the interrupt? */
    if(TMR0IF){
        _ISR_CPS();
        if(countDown){countDown--;}
    }
    
    /* TMR 1 overflows if there is more than 2^16 oscillation in TMR0 interval 
     * If so, take actions to reduce the number of counts
     */
    else if(TMR1IF){
        _ISR_CPS_Overflow();
    }
//    else if (TMR2IF)
//    {
//        TMR2IF = 0;          
//    }
//    else{
//        /* Unhandled interrupts */
//    }

    GIE=1; // enable interrupts
}
