#include "CPS.h"

void init_CPS(void){
    
    /* Setup I/O for CPS
     * (must be set after other user pins, to make sure they are correct)
     */
    TRISA  |= TRISA_CPS;
    ANSELA |= ANSELA_CPS;           // CPS pins must be set analogical input
#if defined(_16F1823)
    TRISC  |= TRISC_CPS;
    ANSELC |= ANSELC_CPS;           // CPS pins must be set analogical input
#endif

    CPS_sel = 0;        //reset the CPS_counter
    
    /* Initialize peripherals */
    CM1CON1            = 0;
    CPSCON0bits.CPSRM  = 0;      // 1=variable voltage ref, 0=fixed
    CPSCON0bits.CPSRNG = 0b11;   // high current
    CPSCON0bits.CPSON  = 1;

    CPSCON1bits.CPSCH  = CPS_List[0];   //first CPS channel selected
       
    //Timer 0 : time base for CPS
    TMR0CS = 0;                  // select Fosc/4
    OPTION_REGbits.PS  = 0b111;  // 1:256 prescaler
    OPTION_REGbits.PSA = 0;      // prescaler assigned to Timer 0 module

    // Timer 1 : frequency counter for CPS
    TMR1   = 0;
    T1CON  = 0b11000101;
         /*
          * bits 7-6 TMR1CS  : 11 is capacitive sensing osc clock source
          * bits 5-4 T1CKPS  : 00 is 1:1 prescaler
          * bit  3   T1OSCEN : 0 is TMR1 dedicated oscillator disabled
          * bit  2   #T1SYNC : 1 is Do not synchronize asynchronous clk input
          * bit  1           : Unimplemented: Read as ‘0’
          * bit  0   TMR1ON  : 1 enables Timer1
          */
    T1GSEL = 0b01;  // set timer 1 gate for TMR0 overflow

    // Enable interrupts
    TMR0    = 0;    // reset TMR0
    TMR0IF  = 0;    // clear TMR0 overflow flag
    TMR1IF  = 0;    // clear TMR1 overflow flag
    TMR0IE  = 1;    // enable TMR0 interrupts
    TMR1IE  = 1;    // enable TMR1 interrupts to manage overflows
    PEIE    = 1;    //enable Peripheral interrupts
    // GIE must be set in user.c after all the peripherals have been initialized
}

void _ISR_CPS(void){
    TMR1ON  = 0;                // turn off timer 1
    update_CPS(&CPS[CPS_sel]);  // takes quite a long time for an ISR... \
        the value of TMR1 could also be stored and used after re-launching TMR1
    TMR1   = 0;                 // clear timer 1
    if(++CPS_sel == CPS_NUM) CPS_sel = 0;   // next channel (with modulo)
    CPSCON1bits.CPSCH = CPS_List[CPS_sel];
    TMR0   = 0;                 //restart timer 0
    T0IF   = 0;
    TMR1ON = 1;                 // enable timer 1
}

void _ISR_CPS_Overflow(void){
    TMR1ON  = 0;
#if OVERFLOW_METH==OVERFLOW_PRESCALER
    //reduce the TMR0 prescaler to make intervals shorter
    if(OPTION_REGbits.PS > 0) OPTION_REGbits.PS--;
#elif OVERFLOW_METH==OVERFLOW_POWER
    //reduce the CPS Power to lower the frequency
    if(CPSCON0bits.CPSRNG > 1) CPSCON0bits.CPSRNG--;
#else
#error *** OVERFLOW_METH not properly defined ***
#endif
    /* Reinitialize CPS channels */
    for(CPS_sel=0;CPS_sel<CPS_NUM;CPS_sel++){
        CPS[CPS_sel].avg = 0;
        CPS[CPS_sel].status = UP;
    }
    CPS_sel = 0;
    CPSCON1bits.CPSCH = CPS_List[0];
    TMR0    = 0;
    TMR0IF  = 0;
    TMR1IF  = 0;
    TMR1ON  = 1;
}

void update_CPS(CPSChannel* cps){
    volatile uint24_t val;
    val = ((uint24_t)TMR1 << CPS_AVGDEEP);
    // If the sensor is DOWN/PRESSED...
    if((cps->status & (1<<0))){
        //...and the value is higher than (average-"security trip")
        if(val > (cps->avg - (cps->avg >> (SENSITIVITY+1)))){
            //go back to UP/RELEASED
            cps->status &= ~(1<<0);
        }
        //...otherwise do nothing and leave
    }
    // Else (if it is UP/RELEASED)
    else{
        // find a drop (based on sensitivity)
        if( (val < cps->avg) && (cps->avg - val) > (cps->avg >> SENSITIVITY) ){
                cps->status = PRESSED;
            }
        //or just continue averaging the main value
        else{
            /* Pseudo-Average */
            cps->avg = cps->avg - (cps->avg >> CPS_AVGDEEP) + (uint24_t)TMR1;
        }
    }
}