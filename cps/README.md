# CPS : Capacitive Sensing peripheral on 8bits Microchip microcontrollers.

- Author : Nicolas H.-P. De Coster (Vigon) 
- Date : 2019-05
- version : v0
- license : [CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/)
- processor : this code will/should work on PIC12F1822, PIC12F1840, PIC16F1823, PIC16F1824, PIC16F1825. See the dedicated section hereafter.
- todo : nothing planned

## Intro
This code was written to provide an easy way to handle _CPS_ (capacitive sensing) in 8bits Microchip microcontrollers. It was written for my personal use but I of course hope it could be useful for others. I'm aware it might not be perfect but I put some effort in keeping it clean, documented and efficient.

That's why I share it under the Creative Commons [CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/) license.

## Which microcontrollers are handled?
At least : 

* PIC12F1822 was tested
* PIC12F1840 was tested
* PIC16F1823 was not tested but is technically the same as 12F1822, with more pins and CPS channels
* PIC16F1824 was not tested but should work
* PIC16F1825 was not tested but should work
* probably other 8bits microcontrollers similar to those listed, equipped with CPS peripheral and having more or less the same configuration...

## How to use it?
All you have to do is :

* to set the _CPS_USEX_ values to 0 (unused) or 1 (used) to tell the code which CPS you want to use
* ajust the sensitivity by setting the _SENSITIVITY_ parameter (from _LOW to _HUGE)
* ajust the _AVG_DEEP_ parameter to set the depth of the pseudo-averaging (don't change it if you don't know what it means or read the documentation hereafter)

Those parameters are located in [CPS.h](CPS.h).

The rest of the user code should be placed in [user.c](user.c) and [main.c](main.c).

The _XTAL_FREQU (oscillator frequency) must be set in [system.h](system.h). Pay attention to the fact that **only HF oscillator values** are currently handled. (cfr the todo list hereafter). A value between 4000000 (_4MHz_) and 500000 (_500kHz_) is currently recommended and must correspond to existing internal oscillator configuration (see datasheet or [system.c](system.c))


## How does it work?

### Physics basics
The idea is quite simple : 

* a constant current is put on the corresponding channel/pin until a defined voltage (ref+) value is reached
* a constant current is then sucked until a defined voltage (ref-) value is reached
* those rise/fall will be counted

The value of that current can be adjusted (high, medium or low) through special registers. In our code, the value is high by default (less prone to interference, better detection) and may be dynamically adapted if the _OVERFLOW_METH_ was set to _OVERFLOW_POWER_. The value of ref+ and ref- are set by internal references (set todo section and the datasheet for more information).

This oscillations are counted by TMR1 and read after a fixed time determined by TMR0 (and TMR0 prescaler).

A drop of the capacitance on the corresponding pin (because it was touched or approached closely) will be detected by a drop of the frequency and thus a lower number of counts on the corresponding channel.

### The code
The code is quite adaptative thanks to PREPROCESSOR operations.

A first initialization phase will set all the corresponding I/O to analog inputs as required, create a list of used channels, and set all the corresponding SFR (special funciton registers) to work with TMR0 and TMR1 and the corresponding interrupts.

Basically, the code is interrupt-driven :

* It will launch a TMR0 and execute main code/loop until TMR0 overflows
* Once TMR0 overflows, it will stop TMR1 to fix the CPS counting
* Then will ask a CPSChannel update to see what to do with the TMR1 value :
  - pseudo-average the value if nothing was detected
  - set the state to PRESSED if a sudden change was detected
  - check when the value goes back to a sufficiently high value (a safe trip which is half the value of the need change is set : see hereafter), otherwise do nothing
* will launch the next CPS acquisition by selecting the next channel (if any) and restarting a TMR0/TMR1 CPS

The fact that the CPS update and check are handled - which are quite demanding operations - inside the ISR (interrupt handling) is a choice not everybody might like but was the result of a trade-off between : 

* as-low-as-possible RAM requirements for CPS itself (not storing TMR1 value, or even worse a current value for each channel)
* as-constant-as-possible time for a CPS acquisition to complete (and not "between acquisitions" thus)
* making sure that any CPS change will be detected, no matter how heavy the main code is

As the code itself probably isn't as time-dependent as the CPS detection is, this choice looked the safest/best. Should this option not meet the user's requirements, the code was written in a way it could be easily adapted...

### Pseudo-averaging
A _uint24_t_ (24bits = 3bytes) _avg_ value will be kept for every CPS channel. When the CPS doesn't trigger (because current value is higher [startup or normal operation] than the average or not sufficiently lower [normal operation]) :

The avg value will be set each time to 

```C
avg = avg - (avg >> AVG_DEEP) + val
```

where:

  * _avg_ is the current pseud-averaged value
  * _AVG_DEEP_ is a division _(1/(2^(AVG_DEEP)))_ factor => _avg - (avg >> AVG_DEEP)_ is thus _(2^(AVG_DEEP)-1)/2^(AVG_DEEP)_ (for example 7/8 if _AVG_DEEP_ = 3)
  * val is the current value

This will make the _avg_ value rise until it is _val*(2^AVG_DEEP)_ , thus _val<<AVG_DEEP_, val supposed being constant. As the current value is represented by the TMR1 value, which is 16bits wide, a 24bits value is required to store it, and the maximum value of _AVG_DEEP_ is 8 (which is more than enough).

The idea is that a change will only affect 1/(2^(AVG_DEEP)) of the current average value and make it change slowly.

### Sensitivity
For a sudden change to trigger the detection, a relative drop of _1/(2^SENSITIVITY)_ of the TMR1 value is required.

To do so, we check that the current value is smaller than the average (rejects easily and quickly any noise over average, and avoid negative/unsigned integers representation problems) and that the difference between "previous value" (represented by the average) and the current value (represented by TMR1) is bigger than _1/(2^SENSITIVITY)_of the "previous value". The higher the _SENSITIVITY_, the smaller the difference should be to trigger a detection, quite logically.

Also, it will require the frequency to go back to _1/(2^(SENSITIVITY+1))_ of avg value higher... wich means that a "safe trip" zone is set at half the threshold. This will create sort of an hysteresis to avoid multiple detection when the drop is very close to the detection threshold.

### CPS states
Four status where defined : 

* 0 = _UP_: No detection, normal operation. Waiting to get "touched".
* 1 = _DOWN_ : the CPS is still touched but the "PRESSED" event has already been handled by the main code. Waiting to go back UP.
* 2 = _RELEASED_ : the CPS isn't touched anymore but the "PRESSED" event was not handled yet by the main code.
* 3 = _PRESSED_ : A sudden change was detected and the CPS is still "touched".

The _bit0_ of status thus represents the current touched[1] or untouched[0] value while the _bit1_ represents whether the CPS was previously touched [1 = CPS was touched].

The CPS will never go _DOWN_ or _UP_ (from _PRESSED_ or _RELEASED_, respectively) via the CPS code : the corresponding bit [_bit1_] should thus be handled by user code to take proper action when a detection occurred.

## Todo
* change the code with macros to set or clear specific bits or check if bit is set/clear to make it easier to read
* calibrate automatically CPS oscillator power so that it remains in reasonable range (but difficult to differentiate between touched-during-startup and high capacitance device, or was intentionally touched CPS). The CPS oscillator already adapts TMR0 interval or CPS power (based on _OVERFLOW_METH_ parameter in [CPS.h](CPS.h) ) in the CPS value is too high. Only a "too low CPS" count should thus still be implemented...
* check whether it's better to use FVR/ADC-based references to keep frequency constant with _VDD_ changes (for battery use, for example). Cfr documentation in datasheets and _CPSCON0bits.CPSRM_ configuration in "user.c". Probably not very useful because of the pseudo-averaging and will require the _FVR_ and _DAC_ peripherals (blocked functions and requires more power)...
