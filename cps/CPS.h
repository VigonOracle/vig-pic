#ifndef CPS_H
#define CPS_H

#include <xc.h>
#include <stdint.h>

/*
 * define sensitivity
 * list (do not modify)
 * A change of 1/(2^SENSITIVITY) is required to trigger
 * For example : SENSITIVITY_GOOD requires 6,25% (=1/2^4) drop of frequency
 * Once triggered, a "trip" of half the threshold is defined 
 *  above which the value should go before being considered released
 */

/*
 * Select sensitivity : 
 *  _LOW, _MED, _GOOD, _HIGH or _HUGE
 */
//~need metal to be touched directly
#define SENSITIVITY_LOW     1
//~need to be touched "firmly" through isolation
#define SENSITIVITY_MED     2
//~need to be approached very close or touched through isolation
#define SENSITIVITY_GOOD    4
//~need to be approached quite close
#define SENSITIVITY_HIGH    6
//~small change will be detected (very sensitive : might trigger too soon)
#define SENSITIVITY_HUGE    8

//select the value here :
#define SENSITIVITY SENSITIVITY_MED



/*
 * Select TMR1 (CPS dedicated) overflow management method
 * 
 */
#define OVERFLOW_PRESCALER  1
#define OVERFLOW_POWER      2
//select here :
#define OVERFLOW_METH OVERFLOW_PRESCALER

/*
 * Define used CPS channels : set 1 to use, 0 to disable
 */
#define USE_CPS0 0
#define USE_CPS1 0
#define USE_CPS2 0
#define USE_CPS3 1
//for bigger microcontrollers only
#define USE_CPS4 0
#define USE_CPS5 0
#define USE_CPS6 0
#define USE_CPS7 0


#define CPS_AVGDEEP 3

/********************
 *  Computed values *
 *  (nothing should not be modified by user under this line)
 ********************/

#if (USE_CPS0 > 1) || (USE_CPS1 > 1) || (USE_CPS2 > 1) || (USE_CPS3 > 1) ||\
    (USE_CPS4 > 1) || (USE_CPS5 > 1) || (USE_CPS6 > 1) || (USE_CPS7 > 1)
#error *** At least one USE_CPSx parameter was not properly defined ***
#endif

//pay attention to the fact that CPS3 is on RA4 (and not RA3)
#define TRISA_CPS   (USE_CPS0<<0)|(USE_CPS1<<1)|(USE_CPS2<<2)|(USE_CPS3<<4)
#define ANSELA_CPS  (USE_CPS0<<0)|(USE_CPS1<<1)|(USE_CPS2<<2)|(USE_CPS3<<3)
#define USED_CPS    (USE_CPS0<<0)|(USE_CPS1<<1)|(USE_CPS2<<2)|(USE_CPS3<<3)|\
                    (USE_CPS4<<4)|(USE_CPS5<<5)|(USE_CPS6<<6)|(USE_CPS7<<7)

#if defined(_12F1822) || defined(_12F1840)
    #define CPS_NUM USE_CPS0+USE_CPS1+USE_CPS2+USE_CPS3
#elif defined(_16F1823)
    #define CPS_NUM USE_CPS0+USE_CPS1+USE_CPS2+USE_CPS3\
                    +USE_CPS4+USE_CPS5+USE_CPS6+USE_CPS7
    #define TRISC_CPS   (USE_CPS4<<0)|(USE_CPS5<<1)|(USE_CPS6<<2)|(USE_CPS7<<3)
    #define ANSELC_CPS  (USE_CPS4<<0)|(USE_CPS5<<1)|(USE_CPS6<<2)|(USE_CPS7<<3)
#endif


typedef struct {
    uint24_t avg; //need uint24 instead of uint16 for pseudo-average
    uint8_t  status;
} CPSChannel;

//construct the list of used CPS based on the USE_CPSX previously defined
const uint8_t CPS_List[CPS_NUM]={
#if USE_CPS0
    0,
#endif
#if USE_CPS1
    1,
#endif
#if USE_CPS2
    2,
#endif
#if USE_CPS3
    3,
#endif
#if USE_CPS4
    4,
#endif
#if USE_CPS5
    5,
#endif
#if USE_CPS6
    6,
#endif
#if USE_CPS7
    7,
#endif
};

CPSChannel  CPS[CPS_NUM];
uint8_t     CPS_sel;


/*CPS states :
 * bit 0 : is up (0) or down (1)
 * bit 1 : was pressed (1) - must be cleared in software to handle state change
 */
#define UP          0
#define DOWN        1
#define RELEASED    2
#define PRESSED     3


/**
 * Functions prototypes
 */

/**
 * void init_CPS(void)
 *  - Initializes the corresponding I/O
 *  - Sets the TMR0 (time base) and TMR1 (CPS)
 *  - Activates corresponding interrupts (and reset flags)
 */
void init_CPS(void);

/**
 * void _ISR_CPS(void)
 *  - Handles TMR0 overflow interrupt in normal CPS operation
 *  - Stops TMR1, read the value, update the corresponding channel
 *  - Then selects next channel and launches a new CPS acquisition
 */
void _ISR_CPS(void);

/**
 * void _ISR_CPS_Overflow(void)
 *  - Handles TMR1 overflow : 
 *      encountered only if TMR0 interval is too long or CPS power too high.
 *  - Takes actions to reduce TMR1 CPS counts by (based on OVERFLOW_METH) :
 *          * either reducing TMR0 interval
 *          * or reducing CPS power mode 
 */
void _ISR_CPS_Overflow(void);

/**
 * void update_CPS(CPSChannel* cps);
 * @param CPSChannel* cps : pointer to the corresponding struct CPSChannel
 *  - updates the corresponding CPS channel
 *  - checks/sets the current state and takes corresponding action by :
 *      * averaging
 *      * setting a trip value to create a safe-zone before returning high
 *      * doing nothing when needed
 */
void update_CPS(CPSChannel* cps);

#endif