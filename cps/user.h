#ifndef USER_H
#define USER_H

#include <xc.h>
#include <stdint.h>

#include "system.h"
#include "CPS.h"

/*
 * Global vars (in addition to .h includes defined)
 */

uint8_t countDown = 0;


/**
 * void init_App(void);
 *  - Sets the user I/O, peripherals and call the other needed initializations
 *  - Should be adapted by user
 */
void init_App(void);

#endif
