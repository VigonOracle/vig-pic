#include "battery_check.h"

bool is_Battery_OK(void){

#if BAT_CHECK_MODE == BAT_KEEP
    volatile uint8_t reg_fvr, reg_adc0, reg_adc1;
    reg_fvr     = FVRCON;
    reg_adc0    = ADCON0;
    reg_adc1    = ADCON1;
#endif    

    // FVREN enabled; TSEN (temp indicator) disabled; CDAFVR off; ADFVR 2x;
    FVRCON = 0b10000010;
    /*
     * bit 7 FVREN: FVR Enable[1]/Disable[0] bit
     * bit 6 FVRRDY: FVR Ready[1]/NotReady[0] Flag bit
     * bit 5 TSEN: Temperature Indicator Enable[1]/Disable[0] bit
     * bit 4 TSRNG: Temperature Indicator Range Selection bit
        * 0 = V OUT = V DD - 2V T (Low Range)
        * 1 = V OUT = V DD - 4V T (High Range)
     * bit 3-2 CDAFVR : Comparator and DAC FVR Selection bits
        * 00 = Comparator, DAC and CPS module FVR out is off
        * 01 = Comparator, DAC and CPS module FVR out is 1x (1.024V)
        * 10 = Comparator, DAC and CPS module FVR out is 2x (2.048V)
        * 11 = Comparator, DAC and CPS module FVR out is 4x (4.096V)
     * bit 1-0 ADFVR<1:0>: ADC FVR Selection bits
        * 00 = ADC FVR out is off
        * 01 = ADC FVR out is 1x (1.024V)
        * 10 = ADC FVR out is 2x (2.048V)
        * 11 = ADC FVR out is 4x (4.096V)
     */
    // ADFM right; ADPREF VDD; ADCS F RC (dedicated osc); 
    ADCON1 = 0b10110000;
        /*
         * bit 7 ADFM   : Left[0]/Right[1] justified
         * bit 6-4 ADCS : adc clock selection
         *      - 000 = F OSC /2
                - 001 = F OSC /8
                - 010 = F OSC /32
                - 011 = F RC (clock supplied from a dedicated RC oscillator)
                - 100 = F OSC /4
                - 101 = F OSC /16
                - 110 = F OSC /64
                - 111 = F RC (clock supplied from a dedicated RC oscillator)
         * bit 3-2      : unimplemented
         * bit 1-0 ADPREF : positive ref voltage
         *      - 00 = V REF + is connected to AV DD
                - 01 = Reserved
                - 10 = V REF + is connected to external V REF + (1)
                - 11 = V REF + is connected to internal FVR module
         */
    // GO_nDONE stop; ADON enabled; CHS AN0; 
    ADCON0 = 0b01111101;
        /*
         * bit 7        : unimplemented
         * bit 6-2 CHS  : channel selection
         *      - 0b00000 to 0b00111 = corresponding AN0-7
         *      - 0b11101 = Temp indicator
         *      - 0b11110 = DAC output
         *      - 0b11111 = FVR buffer 1 output
         * bit 1        : Go/#Done
         * bit 0        : ADC enabled[1]/disabled[0]
         */
    //wait for the FVR to be ready
    while(!FVRCONbits.FVRRDY){}
    //wait at least acquisition time
    __delay_us(ACQ_US_DELAY);
    //go for conversion
    ADCON0bits.nDONE = 1;
    //wait for the conversion to finish
    while (ADCON0bits.GO_nDONE){}
    
#if BAT_CHECK_MODE == BAT_ECO
    FVRCON = 0;
    ADCON0 = 0;
#elif BAT_CHECK_MODE == BAT_KEEP
    FVRCON = reg_fvr;
    ADCON0 = reg_adc0;
    ADCON1 = reg_adc1;
#endif

    //check the limit and return corresponding value
    if(((ADRESH << 8) + ADRESL) > BATTERY_LIMIT){
        return 0;
    }
    else{
        return 1;
    }
}