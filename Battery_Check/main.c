/**
 * B a t t e r y   C h e c k
 *  
 * @summary :
 *    A very simple example to check the voltage status by converting a fixed
 *  voltage reference (@2.048V) with the ADC using Ref- = GND / Ref+ = Vdd. Thus
 *  the lowest Vdd is (min 2.048V), the higher is the ADC conversion result.
 *    This short example checks that the battery voltage is > 3.1V (li-ion):
 *      - If so, it will keep the LED (connected to RA5) high
 *      - Otherwise it will turn the LED off and make it flash shortly to
 *        preserve battery.
 *    This example also uses a Watchdog delay entirely written with PREPROCESSOR
 *    to easily save battery power.
 * 
 * @author : Nicolas H.-P. De Coster (Vigon)
 * @date   : 2019-05
 * @version: v0
 * @processor : this code will/should work on any processor equipped with FVR
 * and ADC. Was tested/created on PIC12F1822.
 * @todo : nothing planned
 */

#include <xc.h>
#include <stdint.h>
#include <stdbool.h>

#include "battery_check.h"
#include "wdt.h"

#define LED RA5

void main(void){
    //all pins digital output
    TRISA = 0;
    ANSELA = 0;
    
    //main loop
    while (1){
        if(is_Battery_OK()){
            //light on
            RA5 = 1;
        }
        else{
            //signal empty battery by short flash [8ms every ~4s]
            RA5 = 1;
            WDT_SLEEP(WDT_8MS);
            RA5 = 0;
        }
        WDT_SLEEP(WDT_4S);
    }
}
