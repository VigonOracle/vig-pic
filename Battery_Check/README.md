# Battery Check

- Author : Nicolas H.-P. De Coster (Vigon) 
- Date : 2019-05
- version : v0
- license : [CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/)
- processor : this code will/should work on any processor equipped with FVR and ADC without major modifications. Was tested/created on PIC12F1822.
- todo : nothing planned

A very simple example to check the voltage status by converting a fixed voltage reference (@2.048V) with the ADC using Ref- = GND / Ref+ = Vdd. Thus the lowest Vdd is (min 2.048V), the higher is the ADC conversion result.
This short example checks that the battery voltage is > 3.1V (li-ion):

- If so, it will keep the LED (connected to RA5) high
- Otherwise it will turn the LED off and make it flash shortly (~8ms every ~4s) to preserve battery.

This example also uses a Watchdog delay entirely written with PREPROCESSOR to easily save battery power.
