#ifndef WDT_H
#define	WDT_H

#include <xc.h> // include processor files - each processor file is guarded.  

#define WDT_1MS     0b00000 //= 1:32      (2^05) (Interval   1 ms typ)
#define WDT_2MS     0b00001 //= 1:64      (2^06) (Interval   2 ms typ)
#define WDT_4MS     0b00010 //= 1:128     (2^07) (Interval   4 ms typ)
#define WDT_8MS     0b00011 //= 1:256     (2^08) (Interval   8 ms typ)
#define WDT_16MS    0b00100 //= 1:512     (2^09) (Interval  16 ms typ)
#define WDT_32MS    0b00101 //= 1:1024    (2^10) (Interval  32 ms typ)
#define WDT_64MS    0b00110 //= 1:2048    (2^11) (Interval  64 ms typ)
#define WDT_128MS   0b00111 //= 1:4096    (2^12) (Interval 128 ms typ)
#define WDT_256MS   0b01000 //= 1:8192    (2^13) (Interval 256 ms typ)
#define WDT_512MS   0b01001 //= 1:16384   (2^14) (Interval 512 ms typ)
#define WDT_1S      0b01010 //= 1:32768   (2^15) (Interval   1  s typ)
#define WDT_2S      0b01011 //= 1:65536   (2^16) (Interval   2  s typ)(Res. val)
#define WDT_4S      0b01100 //= 1:131072  (2^17) (Interval   4  s typ)
#define WDT_8S      0b01101 //= 1:262144  (2^18) (Interval   8  s typ)
#define WDT_16S     0b01110 //= 1:524288  (2^19) (Interval  16  s typ)
#define WDT_32S     0b01111 //= 1:1048576 (2^20) (Interval  32  s typ)
#define WDT_64S     0b10000 //= 1:2097152 (2^21) (Interval  64  s typ)
#define WDT_128S    0b10001 //= 1:4194304 (2^22) (Interval 128  s typ)
#define WDT_256S    0b10010 //= 1:8388608 (2^23) (Interval 256  s typ)

#define WDT_SLEEP(x) CLRWDT(); WDTCON = ( x << 1) | 1; SLEEP(); WDTCONbits.SWDTEN = 0 

#endif