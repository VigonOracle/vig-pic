#ifndef BATTERY_CHECK_H
#define	BATTERY_CHECK_H

#include <xc.h>
#include <stdint.h>
#include <stdbool.h>
#include "system.h"


/*********************
 * BATTERY CHECK MODE
 *********************/
//economic mode : disable ADC/FVR after conversion to spare energy
#define BAT_ECO     1
//preserving mode : save ADC/FVR status before conversion and set it back after
#define BAT_KEEP    2

//select here the mode you want
#define BAT_CHECK_MODE BAT_ECO


/************************
 * BATTERY VOLTAGE LIMIT
 ************************/
// With FVR @ 1.024V x 2 = 2.048V and ADC read as 10bits value
#define BAT_5V5 383
#define BAT_5V0 421
#define BAT_4V5 467
#define BAT_4V0 525
#define BAT_3V7 568
#define BAT_3V5 601
#define BAT_3V3 637
#define BAT_3V2 657
#define BAT_3V1 677
#define BAT_3V0 701
#define BAT_2V5 840

//select here or put an int directly (Value = 2.048V*1024/desired voltage)
//desired voltage must be between 2.048V and max voltage of the processor
#define BATTERY_LIMIT BAT_3V1


/*********
 * OTHERS
 *********/

#define ACQ_US_DELAY 5


/***********************
 * FUNCTIONS PROTOTYPES
 ***********************/

/**
 * bool is_Battery_OK(void);
 *  - checks that the current battery (or power supply) voltage is higher than
 *  a predefined value (see battery_check.h)
 *  - Two modes available :
 *      * BAT_ECO  : will turn ADC and FVR off after battery check to save power
 *      * BAT_KEEP : will save ADC and FVR registers and return them after battery check
 */
bool is_Battery_OK(void);

#endif /* END BATTERY_CHECK_H */
